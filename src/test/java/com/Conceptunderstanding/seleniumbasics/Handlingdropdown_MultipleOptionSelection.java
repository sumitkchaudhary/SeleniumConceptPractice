/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 07-Apr-2022
 *  FILE NAME  		: 	 Handlingdropdown_MultipleOptionSelection.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    10:08:31 am
 */
package com.Conceptunderstanding.seleniumbasics;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.SeleniumConcept.BaseUtilities.MasterControllers;

public class Handlingdropdown_MultipleOptionSelection extends MasterControllers {
	public static void main(String[] args) {
		launchBrowserController();
		
		driver.navigate().to("https://demoqa.com/select-menu");
		driver.manage().window().maximize();
		//WebElement multipleSelectionDropDown=driver.findElement(By.xpath("//*[@id=\"selectMenuContainer\"]/div[7]/div/div"));
		
		Select dropDownOption = new Select(driver.findElement(By.id("oldSelectMenu")));
		List<WebElement> optionList=dropDownOption.getAllSelectedOptions();
		for(WebElement option: optionList) {
			System.out.println(option.getText());
		}
	}

}
