/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 03-Apr-2022
 *  FILE NAME  		: 	 LocatingStrategies_ByXPath.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    10:13:33 pm
 */
package com.Conceptunderstanding.seleniumbasics.locatingStrategies;

import org.openqa.selenium.By;

import com.SeleniumConcept.BaseUtilities.MasterControllers;

public class LocatingStrategies_ByXPath extends MasterControllers {
	
	public static void main(String[] args) {
		
		launchBrowserController();
		driver.navigate().to("https://opensource-demo.orangehrmlive.com/");
		driver.manage().window().maximize();
		//Using Single Slash
		/*Single Slash mechanism is also known as finding elements using Absolute XPath. Single Slash is used to create XPath with absolute XPath i.e. 
		 * the XPath would be created to start selection from the document node/ Start node/ Parent node.*/
		
		String loginSectionTitle=driver.findElement(By.xpath("/html/body/div[1]/div/div[3]/div[2]/div[2]/form/div[1]")).getText();
		System.out.println(loginSectionTitle);
		
		//Using Double Slash
		/*Double Slash mechanism is also known as finding elements using Relative XPath. Double slash is used to create XPath with relative path i.e.
		 *  the XPath would be created to start selection from anywhere within the document. - Search in a whole page (DOM) for the preceding string.*/
		String loginSectionTitleWithDoubleSplash=driver.findElement(By.xpath("//form/div[1]")).getText();
		System.out.println(loginSectionTitleWithDoubleSplash);
		
		//Using Single Attribute
		System.out.println(driver.findElement(By.xpath("//div[@id='divUsername']")).getText());
		
		//Using Multiple Attribute  
		driver.findElement(By.xpath("//input[@id='txtUsername'][@name='txtUsername']")).sendKeys("Double attribute");
		//using or
		/*try {
			System.out.println(driver.findElement(By.xpath("//div[@id='divPassword' or class='textInputContainer']")).getText());
			driver.findElement(By.xpath("//input[@name='txtPassword' and id='txtPassword']")).sendKeys("Using and");
			//using AND
			throw new NoSuchElementException("Using wrong element address please check");
		} catch (NoSuchElementException e) {
			System.out.println(e.getMessage());
		}*/
		
		//Using contains() -- //tag[contains(@attribute,'value')] -- Not mandatory value is exact same
		driver.findElement(By.xpath("//*[contains(@id,'btn')]")).click(); 
		
		// starts-with() --"starts-with()" is used to identify an element, when we are familiar with the attributes value (starting with the specified text) of an element.
		String empatyCredMsg=driver.findElement(By.xpath("//*[starts-with(@id,'spanM')]")).getText(); 
		System.out.println(empatyCredMsg);
		
		
		//Using text() --text() method" is used to identify an element based on the text available on the web page.
		String forgPwdMsg=driver.findElement(By.xpath("//*[text()='Forgot your password?']")).getText();
		System.out.println(forgPwdMsg);
		//Using last()--last() method" selects the last element (of mentioned type) out of all input element present.
	   // System.out.println(driver.findElement(By.xpath("(//div[@id='divPassword']//div)[last()]")).getText());
		
	}

}
