/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 31-Mar-2022
 *  FILE NAME  		: 	 LocatingStrategies_ByCSS.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    5:01:24 pm
 */
package com.Conceptunderstanding.seleniumbasics.locatingStrategies;

import org.openqa.selenium.By;
import org.openqa.selenium.By.ByCssSelector;

import com.SeleniumConcept.BaseUtilities.MasterControllers;

public class LocatingStrategies_ByCSS extends MasterControllers {
	public static void main(String[] args) {
		launchBrowserController();
		driver.navigate().to("https://opensource-demo.orangehrmlive.com/");
		driver.manage().window().maximize();
		/*CSS stands for Cascading Style Sheets. It is a Style Sheet Language which is used to describe the look and formatting of 
		 * a document written in markup language. 
		 * locating web elements through CSS involves use of CSS Selector which identifies an element based on the combination of HTML 
		 * tag, id, class and attributes.
*/
		// The Java Syntax for locating a web element through CSS - Tag and ID Selector is written as:
		//    driver.findElement(By.cssSelector("Tag#Value of id attribute")) 
		
		System.out.println("cssSelector method : Login section heading : "+driver.findElement(By.cssSelector("div#logInPanelHeading")).getText());// div is tag of the DOM and logINPanelheading is an value of the id attribute
		System.out.println("ByCssSelector inner class with id method : Login section heading : "+driver.findElement(ByCssSelector.id("logInPanelHeading")).getText());
		
		System.out.println("ByCssSelector inner class with name method: Username place holder"+driver.findElement(ByCssSelector.className("textInputContainer")).getText());
	
		driver.findElement(By.cssSelector("input#txtUsername")).sendKeys("Admin");
		
		//Tag and Attribute //driver.findElement(By.cssSelector("Tag[Attribute=value]"))  
		driver.findElement(By.cssSelector("input[id=txtPassword]")).sendKeys("admin123");
		
		//Tag, Class and Attribute //driver.findElement(By.cssSelector("tag.class[attribute=value]"))
		driver.findElement(By.cssSelector("input.button[id=btnLogin]")).click(); 
		
		driver.findElement(ByCssSelector.partialLinkText("Welcome")).click();
		
		
		//Sub-String Matches driver.findElement(By.cssSelector("Tag[attribute^=prefix of the string]"))  
		driver.findElement(ByCssSelector.linkText("Logout")).click();
		
	
	}

}
