/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 30-Mar-2022
 *  FILE NAME  		: 	 LocatingStrategies_ByClassStrategies.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    5:13:02 pm
 */
package com.Conceptunderstanding.seleniumbasics.locatingStrategies;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.SeleniumConcept.BaseUtilities.MasterControllers;

public class LocatingStrategies_ByClassStrategies extends MasterControllers {

	public static void main(String[] args) throws InterruptedException {
		/*Locating Strategies By ID
		  Locating Strategies By Name
		  Locating Strategies By Class Name
		  Locating Strategies By Tag Name
		  Locating Strategies By Link Text
		  Locating Strategies By Partial Link Text*/
		launchBrowserController();
		driver.navigate().to("https://opensource-demo.orangehrmlive.com/");
		//<input name="txtUsername" id="txtUsername" type="text">
		driver.findElement(By.id("txtUsername")).sendKeys("Admin");
		//<input name="txtPassword" id="txtPassword" autocomplete="off" type="password">
		driver.findElement(By.name("txtPassword")).sendKeys("admin123 ");

		//<input type="submit" name="Submit" class="button" id="btnLogin" value="LOGIN">
		driver.findElement(By.className("button")).click();

		//<span id="spanMessage">Invalid credentials</span>
		List<WebElement> message=driver.findElements(By.tagName("span"));
		for(WebElement msg: message) {
			if(msg.getText().contains("Invalid credentials")) {
				driver.findElement(By.id("txtUsername")).sendKeys("Admin");
				driver.findElement(By.name("txtPassword")).sendKeys("admin123");
				driver.findElement(By.className("button")).click();
				
				/*<a href="/index.php/time/viewEmployeeTimesheet" target="_self">
                                        <img src="/webres_62443108ee4895.28737568/orangehrmTimePlugin/images/MyTimesheet.png">
                                        <span class="quickLinkText">Timesheets</span>
                                    </a>*/
				driver.findElement(By.linkText("Timesheets")).click();
				driver.navigate().back();
				/*<a href="/index.php/leave/applyLeave" target="_self">
                                        <img src="/webres_62443108ee4895.28737568/orangehrmLeavePlugin/images/ApplyLeave.png">
                                        <span class="quickLinkText">Apply Leave</span>
                                    </a>*/
				driver.findElement(By.partialLinkText("Leave")).click();
				
			}
		}
		Thread.sleep(500);
		driver.quit();
		
		/* Difference between linkText() and partialLinkText() method
		 * Partial link text in Selenium is another way of locating an element via a link. 
		 * The only difference from the link text in Selenium to partial link text is that it does not look 
		 * into the exact match of the string value but works on a partial match. So, in case you are looking 
		 * for a link with a bigger text length, you can get away from using only the partial link text rather 
		 * than the whole link text in Selenium.
		 * */
	}
	
	

}
