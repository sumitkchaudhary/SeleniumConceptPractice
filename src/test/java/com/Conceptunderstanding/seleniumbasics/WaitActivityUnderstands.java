/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 12-Apr-2022
 *  FILE NAME  		: 	 WaitActivityUnderstands.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    5:44:21 pm
 */
package com.Conceptunderstanding.seleniumbasics;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.SeleniumConcept.BaseUtilities.MasterControllers;

public class WaitActivityUnderstands extends MasterControllers {
	/*What are Wait commands in Selenium?
	 * While running Selenium tests, it is common for testers to get the message “Element Not Visible Exception“. 
	 * This appears when a particular web element with which WebDriver has to interact, is delayed in its loading. 
	 * To prevent this Exception, Selenium Wait Commands must be used.
	 * In automation testing, wait commands direct test execution to pause for a certain length of time before moving onto 
	 * the next step. This enables WebDriver to check if one or more web elements are present/visible/enriched/clickable, etc.
	 * 
	 * Why do users need Selenium Wait commands?
	 * 
	 * When a web page loads on a browser, various web elements (buttons, links, images) that someone wants to interact with 
	 * may load at various intervals.
	 * In automated Selenium testing, this causes some trouble when identifying certain elements. 
	 * If an element is not located then the “ElementNotVisibleException” appears. Selenium Wait commands help resolve this issue.
	 * Selenium WebDriver provides three commands to implement waits in tests.
	    Implicit Wait
	    Explicit Wait
	    Fluent Wait
	 * */
	public static void implicitWaitMethod() {
		/*In Selenium Implicit Wait directs the Selenium WebDriver to wait for a certain 
		 * measure of time before throwing an exception. Once this time is set, WebDriver will wait for the element before the exception occurs.
		 * Once the command is in place, Implicit Wait stays in place for the entire duration for which the browser is open. 
		 * It’s default setting is 0, and the specific wait time needs to be set by the following protocol.
		 * */
		launchBrowserController();
		driver.navigate().to("https://demoqa.com/forms");
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		driver.findElement(By.xpath("//span[normalize-space()='Practice Form']")).click();
		
		
	}
	public static void explicitWaitMethod() {
		/*Explicit Wait in Selenium
		 * By using the Explicit Wait command, the WebDriver is directed to wait until a certain condition occurs before proceeding with executing the code.
		 * Setting Explicit Wait is important in cases where there are certain elements that naturally take more time to load.
		 * If one sets an implicit wait command, then the browser will wait for the same time frame before loading every web element. 
		 * This causes an unnecessary delay in executing the test script.
		 * Explicit wait is more intelligent, but can only be applied for specified elements. However, it is an improvement on 
		 * implicit wait since it allows the program to pause for dynamically loaded Ajax elements.*/
		launchBrowserController();
		driver.navigate().to("https://demoqa.com/forms");
		WebDriverWait waitActivity=new WebDriverWait(driver, Duration.ofSeconds(60));
		
		WebElement formMenu=driver.findElement(By.xpath("//span[normalize-space()='Practice Form']"));
		
		waitActivity.until(ExpectedConditions.elementToBeClickable(formMenu)).click();
		
	}
	
	public static void flauntWaitMethod() {
		/*Fluent Wait in Selenium
			Fluent Wait in Selenium marks the maximum amount of time for Selenium WebDriver to wait for a certain condition (web element) becomes visible. 
			It also defines how frequently WebDriver will check if the condition appears before throwing the “ElementNotVisibleException”.
			To put it simply, Fluent Wait looks for a web element repeatedly at regular intervals until timeout happens or until the object is found.
			Fluent Wait commands are most useful when interacting with web elements that can take longer durations to load. This is something that often occurs in Ajax applications.
			While using Fluent Wait, it is possible to set a default polling period as needed. The user can configure the wait to ignore any exceptions during the polling period.
			Fluent waits are also sometimes called smart waits because they don’t wait out the entire duration defined in the code. Instead, the test continues to execute as soon as
			 the element is detected – as soon as the condition specified in .until(YourCondition) method becomes true.
		 * */
		//Declare and initialise a fluent wait
		
		launchBrowserController();
		driver.navigate().to("https://opensource-demo.orangehrmlive.com/");
		driver.manage().window().maximize();
		driver.findElement(By.xpath("//input[@id='txtUsername']")).sendKeys("Admin");
		driver.findElement(By.xpath("//input[@id='txtPassword']")).sendKeys("admin123");
		driver.findElement(By.xpath("//input[@id='btnLogin']")).click();
		driver.findElement(By.xpath("//b[normalize-space()='Buzz']")).click();
		WebElement postBox=driver.findElement(By.xpath("//textarea[@id='createPost_content']"));
		FluentWait wait = new FluentWait(driver);
		//Specify the timout of the wait
		wait.withTimeout(Duration.ofSeconds(60));
		//Sepcify polling time
		wait.pollingEvery(Duration.ofSeconds(60));
		//Specify what exceptions to ignore
		wait.ignoring(NoSuchElementException.class);

		//This is how we specify the condition to wait on.
		//This is what we will explore more in this chapter
		wait.until(ExpectedConditions.visibilityOf(postBox));
		postBox.sendKeys("I'm Automation tester and this is scripted comments.");
		driver.findElement(By.xpath("//input[@id='postSubmitBtn']")).click();
		
		
		
		
	}
	
	public static void main(String[] args) {
		//implicitWaitMethod();
		
		flauntWaitMethod();
	}

}
