/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 05-Apr-2022
 *  FILE NAME  		: 	 Handlingdropdowns_SingleOptionSelect.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    5:59:11 pm
 */
package com.Conceptunderstanding.seleniumbasics;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.SeleniumConcept.BaseUtilities.MasterControllers;

public class Handlingdropdowns_SingleOptionSelect extends MasterControllers {
	
	public static void main(String[] args) {
		launchBrowserController();
		driver.navigate().to("https://demoqa.com/select-menu");
		driver.manage().window().maximize();
		WebElement dropDownMenu=driver.findElement(By.xpath("//select[@id='oldSelectMenu']"));
		Select dropDownOption=new Select(dropDownMenu);
		//Get all the options from the drop down
		List<WebElement> dropdownOptionsList=dropDownOption.getAllSelectedOptions();
		//Iterate option one by one
		for(WebElement option: dropdownOptionsList) {
			//print the option one by one
			System.out.println(option.getText());
		}
		//dropDownOption.selectByIndex(1);//this option will select as per index wise. 
		//dropDownOption.selectByValue("4"); //this option will select as per the value attribute (value="no")
		//dropDownOption.selectByVisibleText("Magenta");//this will select as per the visible text 
		dropDownOption.deselectByVisibleText("Magenta");
		
		
	}

}
