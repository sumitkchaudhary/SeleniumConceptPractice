/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 26-Apr-2022
 *  FILE NAME  		: 	 HandlingIamNotRobot.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    6:29:43 pm
 */
package com.Conceptunderstanding.seleniumbasics;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.SeleniumConcept.BaseUtilities.MasterControllers;

public class HandlingIamNotRobot extends MasterControllers {
	
	public static void main(String[] args) throws InterruptedException {
		launchBrowserController();
		driver.manage().window().maximize();
		driver.get("http://164.52.208.76:9093/account/login");
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
		WebDriverWait waitActivity=new WebDriverWait(driver, Duration.ofSeconds(20));
		waitActivity.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//input[@type='email']")))).sendKeys("1000800000");
		
		waitActivity.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//input[@type='password']")))).sendKeys("Poiu@123#");
		WebElement iFrame=driver.findElement(By.xpath("//iframe[@title='reCAPTCHA']"));
		waitActivity.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(iFrame));
		WebElement checkBox=driver.findElement(By.xpath("//span[@id='recaptcha-anchor']"));
		Thread.sleep(200);
		checkBox.click();
		Thread.sleep(500);
		String checkBoxTickStatus=checkBox.getAttribute("style");
		boolean tickStatus=waitActivity.until(ExpectedConditions.attributeContains(checkBox, "aria-checked", "true"));
		System.out.println("Tick booean status "+tickStatus);
		Thread.sleep(200);
		
		driver.switchTo().defaultContent();
		WebElement loginBtn=driver.findElement(By.xpath("//button[normalize-space()='Log In']"));
		System.out.println(checkBoxTickStatus);
		if(tickStatus==true || checkBoxTickStatus.contains("overflow: visible;") ) {
			waitActivity.until(ExpectedConditions.elementToBeClickable(loginBtn)).click();
		}
	}

}
