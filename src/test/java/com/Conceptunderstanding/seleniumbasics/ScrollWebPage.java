/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 12-Apr-2022
 *  FILE NAME  		: 	 ScrollWebPage.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    3:25:14 pm
 */
package com.Conceptunderstanding.seleniumbasics;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.SeleniumConcept.BaseUtilities.MasterControllers;

public class ScrollWebPage extends MasterControllers {
	
	public static void scrollByMethod() {
		launchBrowserController();
		driver.navigate().to("https://demoqa.com/");
		driver.manage().window().maximize();
		JavascriptExecutor executeJs=(JavascriptExecutor)driver;
		executeJs.executeScript("scrollBy(0, 4500)");
	}
	
	public static void scrollTillTheElementFound() {
		launchBrowserController();
		driver.navigate().to("https://www.javatpoint.com/");
		driver.manage().window().maximize();
		
		WebElement aboutMe=driver.findElement(By.xpath("//img[@alt='India Pin Code']"));
		
		JavascriptExecutor executeJs=(JavascriptExecutor)driver;
		// Scrolling down the page till the element is found
		executeJs.executeScript("arguments[0].scrollIntoView();", aboutMe);
		
	}
	
	public static void scrollDownToTheBottom() {
		launchBrowserController();
		driver.navigate().to("https://www.javatpoint.com/");
		driver.manage().window().maximize();
		
	
		JavascriptExecutor executeJs=(JavascriptExecutor)driver;
		// Scrolling down the page till the element is found
		executeJs.executeScript("window.scrollBy(0,document.body.scrollHeight)");
		
	}
	public static void scrollUsingSindingKeys() {
		launchBrowserController();
		driver.navigate().to("https://www.javatpoint.com/");
		driver.manage().window().maximize();
		driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL, Keys.END);
	
	}
	public static void main(String [] args) {
		scrollUsingSindingKeys();
	}

}
