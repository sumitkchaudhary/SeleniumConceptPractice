/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 06-Apr-2022
 *  FILE NAME  		: 	 HandlingDragandDrop.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    4:19:20 pm
 */
package com.Conceptunderstanding.seleniumbasics.ActionsAPI;

import java.net.MalformedURLException;
import java.net.URL;

import com.practiceproject.toolsqa.basepage.ToolsQA_BaseClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import org.testng.Assert;
import org.testng.annotations.Test;

public class HandlingDragandDrop extends ToolsQA_BaseClass {
	@Test
	public void toolSQAWebDragAndDrop() throws MalformedURLException {
		driver.navigate().to(new URL("https://demoqa.com/droppable/"));

		WebElement source= driver.findElement(By.xpath("//div[@id='draggable']"));
		WebElement destination = driver.findElement(By.xpath("//div[@id='simpleDropContainer']//div[@id='droppable']"));
		actionActivity.dragAndDrop(source, destination).perform();
		String expectedMessageAfterDroopedSuccess=destination.getText();
		Assert.assertEquals("Dropped!", expectedMessageAfterDroopedSuccess);
	}
}
