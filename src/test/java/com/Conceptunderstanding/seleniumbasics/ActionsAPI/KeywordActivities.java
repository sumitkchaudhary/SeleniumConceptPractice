package com.Conceptunderstanding.seleniumbasics.ActionsAPI;

import com.practiceproject.toolsqa.basepage.ToolsQA_BaseClass;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class KeywordActivities extends ToolsQA_BaseClass {
    @Test
    public void fillFormByKeyword() throws InterruptedException {
        Thread.sleep(500);
        driver.get("https://demoqa.com/text-box");
        driver.findElement(By.xpath("//input[@id='userName']")).sendKeys("Sumit Kumar");
        driver.findElement(By.xpath("//input[@id='userEmail']")).sendKeys("Test@gmail.com");
        WebElement currentAddressElement=driver.findElement(By.xpath("//textarea[@id='currentAddress']"));
        currentAddressElement.sendKeys("3 School Lane London EC71 9GO");
        // Select the Current Address using CTRL + A
        actionActivity.keyDown(Keys.CONTROL);
        actionActivity.sendKeys("a");
        actionActivity.keyUp(Keys.CONTROL);
        actionActivity.build().perform();
        // Copy the Current Address using CTRL + C
        actionActivity.keyDown(Keys.CONTROL);
        actionActivity.sendKeys("c");
        actionActivity.keyUp(Keys.CONTROL);
        actionActivity.build().perform();
        //Press the TAB Key to Switch Focus to Permanent Address

        actionActivity.keyDown(Keys.TAB);
        actionActivity.build().perform();
        //Paste the Address in the Permanent Address field using CTRL + V

        actionActivity.keyDown(Keys.CONTROL);
        actionActivity.sendKeys("v");
        actionActivity.keyUp(Keys.CONTROL);
        actionActivity.build().perform();

        WebElement permanentAddress=driver.findElement(By.id("permanentAddress"));
        Assert.assertEquals(currentAddressElement.getAttribute("value"),permanentAddress.getAttribute("value"));
    }
}
