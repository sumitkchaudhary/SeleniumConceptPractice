/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 14-Apr-2022
 *  FILE NAME  		: 	 MouseActions.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    1:36:25 pm
 */
package com.Conceptunderstanding.seleniumbasics.ActionsAPI;

import com.practiceproject.toolsqa.basepage.ToolsQA_BaseClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
//import org.openqa.selenium.interactions.internal.MouseAction;

import com.SeleniumConcept.BaseUtilities.MasterControllers;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class MouseActions extends ToolsQA_BaseClass {
	@BeforeTest
	public void commonActions(){
		WebElement elementClick=driver.findElement(By.xpath("//div[@class='category-cards']//div[1]//div[1]//div[2]//*[name()='svg']"));
		//click by mouse
		actionActivity.moveToElement(elementClick).click().perform();
		WebElement buttons=driver.findElement(By.xpath("//span[normalize-space()='TSQA_Buttons']"));
		//mouse over
		actionActivity.moveToElement(buttons).build().perform();
		actionActivity.click(buttons).perform();

	}
	@Test
	public void validateDoubleClickByMouseActions() {
		actionActivity.doubleClick(driver.findElement(By.xpath("//button[@id='doubleClickBtn']"))).perform();
		String doubleClickMsg=driver.findElement(By.xpath("//p[@id='doubleClickMessage']")).getText();
		Assert.assertEquals("You have done a double click",doubleClickMsg);
	}
	@Test
	public void validateRightClickByMouseActions(){
		actionActivity.contextClick(driver.findElement(By.xpath("//button[@id='rightClickBtn']"))).perform();

		String rightclickMsg=driver.findElement(By.xpath("//p[@id='rightClickMessage']")).getText();
		Assert.assertEquals("You have done a right click",rightclickMsg);

	}

}
