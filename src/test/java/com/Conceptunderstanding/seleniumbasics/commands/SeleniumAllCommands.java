/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 29-Mar-2022
 *  FILE NAME  		: 	 SeleniumAllCommands.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    1:31:45 pm
 */
package com.Conceptunderstanding.seleniumbasics.commands;

import com.practiceproject.toolsqa.basepage.ToolsQA_BaseClass;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;

import com.SeleniumConcept.BaseUtilities.MasterControllers;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import java.util.List;

public class SeleniumAllCommands extends ToolsQA_BaseClass {
	@Test
	public void validateCommnad() {
		driver.navigate().to("https://demoqa.com/books");//navigate command redirect to the desire web url
		String webSiteTitle=driver.getTitle(); //getTitle will fetch the web page title and return in string formate
		System.out.println(webSiteTitle); //return ToolsQA
		driver.manage().window().maximize(); //Maximizes the current window if it is not already maximized
		driver.navigate().refresh(); //refresh the current web page
		String currentPageURL=driver.getCurrentUrl(); //getCurrentUrl() method fetch the current page url to verify
		System.out.println(currentPageURL);
		List<WebElement> bookList=driver.findElements(By.xpath("//div[@class='rt-tr-group']//a")); //findElements get the all element which is match the given element address
		for(WebElement bookName: bookList) {
			System.out.println(bookName.getText()); //getText fetch the element text 
		}
		driver.navigate().back(); //Return back the previous screen
		driver.navigate().forward(); // redirect to the forward from current screen to the next screen
		//driver.manage().deleteAllCookies(); //delete the cookies
		driver.close();//redirect to the forward from current screen to the next screen
		driver.quit();//Quits this driver, closing every associated window.
		driver.manage().window().minimize(); //Minimizes the current window if it is not already minimized
		driver.manage().window().fullscreen();////navigate command redirect to the desire web url
		Point windowPosition=driver.manage().window().getPosition();//Get the position of the current window, relative to the upper left corner of the screen.
		System.out.println("Window x not "+windowPosition.getX());//Window x not 9
		System.out.println("Window y not "+windowPosition.getY()); //Window y not 9
		Dimension dimension=driver.manage().window().getSize();//Get the size of the current window. This will return the outer window dimension, not justthe view port.
		System.out.println("Window Height "+dimension.getHeight());//Window Height 798
		System.out.println("Window Width "+dimension.getWidth());//Window Width 1051
	}

}
