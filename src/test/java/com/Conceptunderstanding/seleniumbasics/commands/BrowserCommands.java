/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 31-Mar-2022
 *  FILE NAME  		: 	 BrowserCommands.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    1:34:57 pm
 */
package com.Conceptunderstanding.seleniumbasics.commands;

import com.SeleniumConcept.BaseUtilities.MasterControllers;
import com.practiceproject.toolsqa.basepage.ToolsQA_BaseClass;
import org.testng.annotations.Test;

import javax.tools.Tool;

public class BrowserCommands extends ToolsQA_BaseClass {
	@Test
	public void validateBrowserCommand() {
		String webSiteTitle=driver.getTitle(); //getTitle will fetch the web page title and return in string formate
		System.out.println(webSiteTitle); //return ToolsQA
		String currentPageURL=driver.getCurrentUrl(); //getCurrentUrl() method fetch the current page url to verify
		System.out.println(currentPageURL);
		String webpageSourceCode=driver.getPageSource(); //In WebDriver, this method returns the source code of the current web page loaded on the current browser. 
														 //It accepts nothing as parameter and returns a String value.
		System.out.println(webpageSourceCode);
		driver.close();//redirect to the forward from current screen to the next screen
		driver.quit();//Quits this driver, closing every associated window.
	}
}
