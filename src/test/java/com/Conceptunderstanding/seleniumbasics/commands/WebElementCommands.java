/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 31-Mar-2022
 *  FILE NAME  		: 	 WebElementCommands.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    1:41:54 pm
 */
package com.Conceptunderstanding.seleniumbasics.commands;

import org.openqa.selenium.By;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebElement;

import com.SeleniumConcept.BaseUtilities.MasterControllers;

public class WebElementCommands extends MasterControllers {

	public static void main(String[] args) {
		launchBrowserController();
		driver.navigate().to("https://opensource-demo.orangehrmlive.com");
		driver.manage().window().maximize();
		WebElement userName=driver.findElement(By.id("divUsername"));
		//Question :  What is the width and height of the rendered element?
		System.out.println("Username Inputbox height is: "+userName.getSize().getHeight() +"\n Username Inputbox width is: "+userName.getSize().getWidth());// Fetch the element size. (232, 16)
		System.out.println("Username input box place holder is: "+userName.getText());//Fetch text which in mentioned between the tag block
		System.out.println("User name id attribute value is "+userName.getAttribute("id")); //this will return attribute value of the given attribute
		System.out.println("User name ccs value is : "+userName.getCssValue("class"));//
		boolean checkDisplayedStatus=userName.isDisplayed();
		System.out.println("Check the element display or not in the DOM: "+checkDisplayedStatus);
		boolean checkEnableStatus=userName.isEnabled();
		System.out.println("Input box enable or not: "+checkEnableStatus);
		System.out.println("Tag name is: "+userName.getTagName()); //get element tag nam
		WebElement password=driver.findElement(By.id("txtPassword"));//help to send the desire string to the located element
		password.sendKeys("user");
		Point location=password.getLocation();
		System.out.println("Password input text location X node is: "+location.getX());
		System.out.println("Password input text location Y node is: "+location.getY());
		password.clear(); // help to clear the text
		WebElement loginBtn=driver.findElement(By.className("button"));
		loginBtn.submit();
		
		/*
		userName.getAriaRole();
		
		userName.getDomAttribute("");
		userName.getDomProperty("");
		userName.getRect();
		userName.getShadowRoot();
		userName.isSelected();
		
		*/
		
	}

}
