/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 12-Apr-2022
 *  FILE NAME  		: 	 HandlingDraggable.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    12:29:47 pm
 */
package com.practiceproject.jqueryui.DatePicker;

import java.net.MalformedURLException;
import java.net.URL;

import com.practiceproject.toolsqa.basepage.ToolsQA_BaseClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import org.testng.annotations.Test;

public class HandlingDraggable extends ToolsQA_BaseClass {
	@Test
	public  void checkDraggable() throws MalformedURLException {
		launchBrowserController();
		driver.navigate().to(new URL("https://jqueryui.com/draggable/"));
		driver.manage().window().maximize();
		driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@class='demo-frame']")));
		
		WebElement source=driver.findElement(By.xpath("//div[@id='draggable']"));
		//style="position: relative; left: 379px; top: 216px;"
		
		Actions ac=new Actions(driver);
		ac.dragAndDropBy(source, 379, 216).perform(); //Drag and drop by X and Y node. 
		
	}

}
