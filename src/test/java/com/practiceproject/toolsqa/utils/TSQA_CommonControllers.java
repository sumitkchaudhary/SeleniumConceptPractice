/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 19-Apr-2022
 *  FILE NAME  		: 	 TSQA_CommonControllers.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    2:21:37 pm
 */
package com.practiceproject.toolsqa.utils;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.Months;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TSQA_CommonControllers {
	WebDriverWait waitActivity;
	WebDriver driver;
	public TSQA_CommonControllers(WebDriverWait waitActivity, WebDriver driver) {
		this.waitActivity=waitActivity;
		this.driver=driver;
	}
	//Use for verify the downloaded file
	public static boolean isFileDownloadingSuccessByName(String downloadedFileDirectoryPath, String downloadedFileName) {
		boolean fileExistStatus=false;
		File directory=new File(downloadedFileDirectoryPath);
		File [] dir_Containts=directory.listFiles();
		for(File dir: dir_Containts) {
			if(dir.getName().equals(downloadedFileName)) {
				System.out.println(dir.getName());
				return fileExistStatus=true;
			}
			
		}
		return fileExistStatus;
	}
	public static boolean isFileDownloaded_Ext(String dirPath, String ext){
		boolean flag=false;
	    File dir = new File(dirPath);
	    File[] files = dir.listFiles();
	    if (files == null || files.length == 0) {
	        flag = false;
	    }
	    
	    for (int i = 1; i < files.length; i++) {
	    	if(files[i].getName().contains(ext)) {
	    		flag=true;
	    	}
	    }
	    return flag;
	}
	
	public static void verifyLinks(String linkUrl) {
        try {
            URL url = new URL(linkUrl);

            //Now we will be creating url connection and getting the response code
            HttpURLConnection httpURLConnect=(HttpURLConnection)url.openConnection();
            httpURLConnect.setConnectTimeout(5000);
            httpURLConnect.connect();
            if(httpURLConnect.getResponseCode()>=400){
            	System.out.println("HTTP STATUS - " + httpURLConnect.getResponseMessage() + " is a broken link: "+linkUrl);
            }
            //Fetching and Printing the response code obtained
            else{
                System.out.println("HTTP STATUS - " + httpURLConnect.getResponseMessage()+" "+linkUrl);
            }
        }catch (Exception e) {
        	e.getStackTrace();
        	e.getMessage();
      }
   }
	public static void verifyLinkActive(String linkUrl) {
        try {
           URL url = new URL(linkUrl);
           HttpURLConnection httpURLConnect=(HttpURLConnection)url.openConnection();
           httpURLConnect.setConnectTimeout(3000);
           httpURLConnect.connect();
           if(httpURLConnect.getResponseCode()==200) {
               System.out.println(linkUrl+" - "+httpURLConnect.getResponseMessage());
            }
          if(httpURLConnect.getResponseCode()==HttpURLConnection.HTTP_NOT_FOUND) {
               System.out.println(linkUrl+" - "+httpURLConnect.getResponseMessage() + " - "+ HttpURLConnection.HTTP_NOT_FOUND);
            }
        } catch (Exception e) {
           
        }
    }  
	
	public static void validLink(String url) {
		try {
			HttpURLConnection huc = (HttpURLConnection)(new URL(url).openConnection());

			huc.setRequestMethod("HEAD");

			huc.connect();

			int respCode = huc.getResponseCode();

			if(respCode >= 400){
			System.out.println(url+" is a broken link");
			}
			else{
			System.out.println(url+" is a valid link");
			}

			} catch (MalformedURLException e) {
			e.printStackTrace();
			} catch (IOException e) {
			e.printStackTrace();
			}
	}
	public static boolean checkLeapYear(int  year) {
		   boolean leap = false;

		    // if the year is divided by 4
		    if (year % 4 == 0) {

		      // if the year is century
		      if (year % 100 == 0) {

		        // if year is divided by 400
		        // then it is a leap year
		        if (year % 400 == 0)
		          leap = true;
		        else
		          leap = false;
		      }
		      
		      // if the year is not century
		      else
		        leap = true;
		    }
		    
		    else
		      leap = false;

		    if (leap)
		      return leap;
		    else
		      return leap;
		  }
}
