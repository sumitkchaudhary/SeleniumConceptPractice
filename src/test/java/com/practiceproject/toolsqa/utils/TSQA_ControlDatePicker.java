/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 19-Apr-2022
 *  FILE NAME  		: 	 TSQA_ControlDatePicker.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    4:23:36 pm
 */
package com.practiceproject.toolsqa.utils;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import com.practiceproject.toolsqa.basepage.ToolsQA_BaseClass;

public class TSQA_ControlDatePicker extends ToolsQA_BaseClass{
	
	public static void desireDatePicker(String exMonth, String exDay, String exYear) {
		try {
			Thread.sleep(200);
			new Select(driver.findElement(By.xpath("//select[@class='react-datepicker__month-select']"))).selectByVisibleText(exMonth);
			Thread.sleep(200);
			new Select(driver.findElement(By.xpath("//select[@class='react-datepicker__year-select']"))).selectByVisibleText(exYear);
			Thread.sleep(200);
			driver.findElement(By.xpath("//*[text()='"+exDay+"']")).click();
		} catch (Exception e) {
			System.out.println("You privide wrong date "+"Date :"+exDay+" Month : "+exMonth);
		}
	}
}
