/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 12-Apr-2022
 *  FILE NAME  		: 	 TSQA_HandlingAlerts.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    12:57:58 pm
 */
package com.practiceproject.toolsqa.testcases.alertframewindow;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.practiceproject.toolsqa.basepage.ToolsQA_BaseClass;

public class TSQA_HandlingAlerts extends ToolsQA_BaseClass {
	
	@Test
	public void verifyOkButtonAlert(){
		driver.get("https://demoqa.com/alerts");
		//click alert 
		driver.findElement(By.xpath("//button[@id='alertButton']")).click();
		String clickbtnAlertText=driver.switchTo().alert().getText();
		System.out.println(clickbtnAlertText);
		if(clickbtnAlertText.contains("You clicked a button")) {
			driver.switchTo().alert().accept();
		}
		
	}
	@Test
	public void verifyTimmerAlert(){
		driver.get("https://demoqa.com/alerts");
		//timer alert 
		WebDriverWait waitActivity=new WebDriverWait(driver, Duration.ofSeconds(10));
		driver.findElement(By.xpath("//button[@id='timerAlertButton']")).click();
		waitActivity.until(ExpectedConditions.alertIsPresent());
		String clickbtnAlertText=driver.switchTo().alert().getText();
		System.out.println(clickbtnAlertText);
		if(clickbtnAlertText.contains("This alert appeared after 5 seconds")) {
			driver.switchTo().alert().accept();
		}	
	}
	
	@Test
	public void verifyConfirmationAlert(){
		driver.get("https://demoqa.com/alerts");
		//Confirmation alert
		WebElement confirmationAlertBtn=driver.findElement(By.xpath("//button[@id='confirmButton']"));
		confirmationAlertBtn.click();
		String alertText=driver.switchTo().alert().getText();
		System.out.println(alertText);
		if(alertText.contains("Do you confirm action?")) {
			driver.switchTo().alert().accept();
			WebElement alertConfirmationMsg=driver.findElement(By.xpath("//span[@id='confirmResult']"));
			if(alertConfirmationMsg.getText().contains("You selected Ok")) {
				System.out.println(alertConfirmationMsg.getText());
				confirmationAlertBtn.click();
				driver.switchTo().alert().dismiss();
				if(alertConfirmationMsg.getText().contains("You selected Cancel")) {
					System.out.println(alertConfirmationMsg.getText());
					System.out.println("Pass: Test pass");
				}
			}
		}
	}
	@Test
	public void verifyInputBoxAlert(){
		driver.get("https://demoqa.com/alerts");
		driver.findElement(By.xpath("//button[@id='promtButton']")).click();
		String promptText="Sumit Kumar";
		driver.switchTo().alert().sendKeys(promptText);
		driver.switchTo().alert().accept();
		String promptResult=driver.findElement(By.xpath("//span[@id='promptResult']")).getText();
		System.out.println(promptResult);
		
		if(promptResult.contains("You entered "+promptText)) {
			System.out.println("Pass: Test Pass");
		}else {
			System.out.println("Failed: Test Failed");
		}
	}
}
