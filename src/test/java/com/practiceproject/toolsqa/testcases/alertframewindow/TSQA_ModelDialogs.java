/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 22-Apr-2022
 *  FILE NAME  		: 	 TSQA_ModelDialogs.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    11:44:38 am
 */
package com.practiceproject.toolsqa.testcases.alertframewindow;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.practiceproject.toolsqa.basepage.ToolsQA_BaseClass;

public class TSQA_ModelDialogs extends ToolsQA_BaseClass{
	
	@Test
	public void verifySmallDialog() {
		driver.get("https://demoqa.com/modal-dialogs");
		driver.findElement(By.xpath("//button[@id='showSmallModal']")).click();
		String text=driver.findElement(By.xpath("//div[@class='modal-body']")).getText();
		System.out.println(text);
		driver.findElement(By.xpath("//button[@id='closeSmallModal']")).click();
	}
	@Test
	public void verifyLargeDialog() {
		driver.get("https://demoqa.com/modal-dialogs");
		driver.findElement(By.xpath("//button[@id='showLargeModal']")).click();
		String text=driver.findElement(By.xpath("//div[@class='modal-body']")).getText();
		System.out.println(text);
		driver.findElement(By.xpath("//button[@id='closeLargeModal']")).click();
	}

}
