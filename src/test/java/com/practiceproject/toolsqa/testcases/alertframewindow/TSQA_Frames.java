/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 22-Apr-2022
 *  FILE NAME  		: 	 TSQA_Frames.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    10:51:47 am
 */
package com.practiceproject.toolsqa.testcases.alertframewindow;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import com.practiceproject.toolsqa.basepage.ToolsQA_BaseClass;

public class TSQA_Frames extends ToolsQA_BaseClass{
	
	@Test
	public void verifyFrame() {
		driver.get("https://demoqa.com/frames");
		
		WebElement frame=driver.findElement(By.xpath("//iframe[@id='frame2']"));//iframe[@id='frame1']
		waitActivity.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(frame));
		String frameWindowText=driver.findElement(By.xpath("//*[@id='sampleHeading']")).getText();
		System.out.println(frameWindowText);
	}

}
