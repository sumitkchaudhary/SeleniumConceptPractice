/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 21-Apr-2022
 *  FILE NAME  		: 	 TSQA_BrowserWindow.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    6:16:24 pm
 */
package com.practiceproject.toolsqa.testcases.alertframewindow;

import java.util.Arrays;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WindowType;
import org.testng.annotations.Test;

import com.practiceproject.toolsqa.basepage.ToolsQA_BaseClass;

public class TSQA_BrowserWindow extends ToolsQA_BaseClass {
	
	@Test
	public void verifyNewTab() {
		driver.get("https://demoqa.com/browser-windows");
		driver.findElement(By.xpath("//button[@id='tabButton']")).click();
		Set<String> windows= driver.getWindowHandles();
		for(String win: windows) {
			String url=driver.switchTo().window(win).getCurrentUrl();
			if(url.contains("https://demoqa.com/sample")) {
				String newTabBodyText=driver.findElement(By.xpath("//h1[@id='sampleHeading']")).getText();
				if(newTabBodyText.equals("This is a sample page")) {
					
					System.out.println(newTabBodyText+" Pass: New Tab Open and close the new tab successfully");
				}
				driver.close();
				
			}
		}
	}
	@Test
	public void verifyNewWindowOverExistingWindow() {
		driver.get("https://demoqa.com/browser-windows");
		driver.findElement(By.xpath("//button[@id='windowButton']")).click();
		Set<String> windows= driver.getWindowHandles();
		for(String win: windows) {
			String url=driver.switchTo().window(win).getCurrentUrl();
			if(url.contains("https://demoqa.com/sample")) {
				String newTabBodyText=driver.findElement(By.xpath("//h1[@id='sampleHeading']")).getText();
				if(newTabBodyText.equals("This is a sample page")) {
					
					System.out.println(newTabBodyText+" Pass: New Window open and close the new window successfully");
				}
				driver.close();
				
			}
		}
	}
	@Test
	public void verifyNewMessageWindowOverExistingWindow() {
		driver.get("https://demoqa.com/browser-windows");
		String currentWindow=driver.getWindowHandle();
		driver.findElement(By.xpath("//button[@id='messageWindowButton']")).click();
	
		for(String win: driver.getWindowHandles()) {
				if(!currentWindow.contains(win)) {
					driver.switchTo().window(win).close();
					break;
				}
			}
	}

}
