/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 22-Apr-2022
 *  FILE NAME  		: 	 TSQA_NestedFrame.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    11:15:58 am
 */
package com.practiceproject.toolsqa.testcases.alertframewindow;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import com.practiceproject.toolsqa.basepage.ToolsQA_BaseClass;

public class TSQA_NestedFrame extends ToolsQA_BaseClass{
	
	@Test
	public void verifyNestedFrame() {
		driver.get("https://demoqa.com/nestedframes");
		WebElement parentFrame=driver.findElement(By.xpath("//iframe[@id='frame1']"));
		waitActivity.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(parentFrame));
		String parentFrameText=driver.findElement(By.tagName("body")).getText();
		if(parentFrameText.contains("Parent frame")){
			System.out.println(parentFrameText);
			WebElement childFrame=driver.findElement(By.xpath("//iframe[@srcdoc='<p>Child Iframe</p>']"));
			waitActivity.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(childFrame));
			String childFrameText=driver.findElement(By.tagName("body")).getText();
			if(childFrameText.contains("Child Iframe")) {
				System.out.println("Test Pass :"+childFrameText);
			}
		}
	}
	
	@Test
	public void verifyNestedFrameSwitchToMethod() {
		driver.get("https://demoqa.com/nestedframes");
		WebElement parentFrame=driver.findElement(By.xpath("//iframe[@id='frame1']"));
		driver.switchTo().frame(parentFrame);
		String parentFrameText=driver.findElement(By.tagName("body")).getText();
		if(parentFrameText.contains("Parent frame")){
			System.out.println(parentFrameText);
			WebElement childFrame=driver.findElement(By.xpath("//iframe[@srcdoc='<p>Child Iframe</p>']"));
			driver.switchTo().frame(childFrame);
			String childFrameText=driver.findElement(By.tagName("body")).getText();
			if(childFrameText.contains("Child Iframe")) {
				System.out.println("Test Pass :"+childFrameText);
			}
		}
	}
}
