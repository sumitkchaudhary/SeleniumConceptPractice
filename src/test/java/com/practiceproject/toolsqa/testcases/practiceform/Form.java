/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 19-Apr-2022
 *  FILE NAME  		: 	 Form.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    2:58:00 pm
 */
package com.practiceproject.toolsqa.testcases.practiceform;

import java.io.File;
import java.util.List;

import com.SeleniumConcept.BaseUtilities.MasterControllers;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import com.practiceproject.toolsqa.basepage.ToolsQA_BaseClass;
import com.practiceproject.toolsqa.utils.TSQA_ControlDatePicker;

public class Form extends ToolsQA_BaseClass{
	@Test
	public void formHandle() throws InterruptedException {
		driver.get("https://demoqa.com/automation-practice-form");
		driver.findElement(By.xpath("//input[@id='firstName']")).sendKeys("Sumit");
		driver.findElement(By.xpath("//input[@id='lastName']")).sendKeys("Kumar");
		driver.findElement(By.xpath("//input[@id='userEmail']")).sendKeys("sumit@gmal.com");
		driver.findElement(By.xpath("//label[@for='gender-radio-1']")).click();
		driver.findElement(By.xpath("//input[@id='userNumber']")).sendKeys("9945789245");
		driver.findElement(By.xpath("//input[@id='dateOfBirthInput']")).click();
		TSQA_ControlDatePicker.desireDatePicker("17", "June", "1990");
		selectSubjectByName("English");
		selectSubjectByName("Hindi");
		selectSubjectByName("Math");
		
		List<WebElement> hobbiesList=driver.findElements(By.xpath("//input[contains(@id,'hobbies')]"));
		for(WebElement hobbie: hobbiesList) {
			//System.out.println(hobbie.getText());
			hobbie.sendKeys(Keys.SPACE);
		}

		driver.findElement(By.xpath("//input[@id='uploadPicture']")).sendKeys(MasterControllers.getAbsolutPath("img.png"));
		driver.findElement(By.xpath("//textarea[@id='currentAddress']")).sendKeys("Noida, UP, India");
		WebElement state=driver.findElement(By.xpath("//*[text()='Select State']"));
		waitActivity.until(ExpectedConditions.visibilityOf(state));
		state.sendKeys("Uttar");
		Thread.sleep(500);
		state.sendKeys(Keys.ARROW_DOWN);
		Thread.sleep(500);
		state.sendKeys(Keys.ENTER);
		// TODO: handle the dropdown
		WebElement city=driver.findElement(By.xpath("//div[contains(text(),'Select City')]"));
		city.click();
		city.sendKeys(Keys.ARROW_DOWN);
		city.sendKeys(Keys.ENTER);
		driver.findElement(By.xpath("//button[@id='submit']")).click();
	}
	
	public static void selectSubjectByName(String exSubjectName) {
		WebElement enterSubject=driver.findElement(By.xpath("//input[@id='subjectsInput']"));
		enterSubject.sendKeys(exSubjectName);
		enterSubject.sendKeys(Keys.ARROW_DOWN);
		enterSubject.sendKeys(Keys.ENTER);
		
	}

}
