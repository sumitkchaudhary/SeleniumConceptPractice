/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 16-Apr-2022
 *  FILE NAME  		: 	 TSQA_RadioButtons.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    12:18:19 am
 */
package com.practiceproject.toolsqa.testcases.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import com.practiceproject.toolsqa.basepage.ToolsQA_BaseClass;

public class TSQA_RadioButtons extends ToolsQA_BaseClass{
	
	@Test
	public void handleRadioButtons() {
		waitActivity.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[name()='path' and contains(@d,'M16 132h41')]")))).click();
		driver.findElement(By.xpath("//div[@class='element-list collapse show']//li[@id='item-2']")).click();
		driver.findElement(By.xpath("//label[@for='impressiveRadio']")).click();
		String successMsg=driver.findElement(By.xpath("//span[@class='text-success']")).getText();
		
		if(successMsg.contains("Impressive")) {
			System.out.println("Pass");
		}else {
			System.out.println("Failed");
		}
	}

}
