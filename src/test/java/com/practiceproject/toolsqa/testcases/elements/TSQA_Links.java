/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 16-Apr-2022
 *  FILE NAME  		: 	 TSQA_Links.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    5:52:51 pm
 */
package com.practiceproject.toolsqa.testcases.elements;

import java.io.IOException;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WindowType;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;


import com.practiceproject.toolsqa.basepage.ToolsQA_BaseClass;
import com.practiceproject.toolsqa.utils.TSQA_CommonControllers;

public class TSQA_Links extends ToolsQA_BaseClass{
	
	@Test
	public void handleLinks(){
		driver.get("https://demoqa.com/links");
		String currentTab=driver.getWindowHandle();
		driver.findElement(By.xpath("//a[@id='simpleLink']")).click();
		driver.switchTo().window(currentTab);		
		driver.findElement(By.xpath("//a[@id='simpleLink']")).click();
		
		for(String windowHandle : driver.getWindowHandles()) {
			if(!currentTab.contentEquals(windowHandle)) {
				driver.switchTo().window(windowHandle).close();
				break;
			}
		}
	}
	
	@Test
	public void verifyTheValidLinks() throws ClientProtocolException, IOException {
		// waitActivity.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[name()='path' and contains(@d,'M16 132h41')]")))).click();
		driver.get("https://demoqa.com/links");
		//waitActivity.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[text()='Links']")))).click();
		List<WebElement> linksList=driver.findElements(By.xpath("//div[@id='linkWrapper']//a"));
		
		for(WebElement link: linksList) {
				TSQA_CommonControllers.verifyLinkActive(link.getAttribute("href"));	
		}	
	}
	

}