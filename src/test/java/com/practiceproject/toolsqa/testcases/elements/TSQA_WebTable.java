/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 16-Apr-2022
 *  FILE NAME  		: 	 TSQA_WebTable.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    12:24:11 am
 */
package com.practiceproject.toolsqa.testcases.elements;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import com.practiceproject.toolsqa.basepage.ToolsQA_BaseClass;

public class TSQA_WebTable extends ToolsQA_BaseClass{
	
	@Test(priority = 1)
	public void addNewRecords() {
		waitActivity.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[name()='path' and contains(@d,'M16 132h41')]")))).click();
		driver.findElement(By.xpath("//*[text()='Web Tables']")).click();
		driver.findElement(By.xpath("//button[@id='addNewRecordButton']")).click();
		driver.findElement(By.xpath("//input[@id='firstName']")).sendKeys("Sumit");	
		driver.findElement(By.xpath("//input[@id='lastName']")).sendKeys("Kumar");	
		driver.findElement(By.xpath("//input[@id='userEmail']")).sendKeys("Sumit@test.com");	
		driver.findElement(By.xpath("//input[@id='age']")).sendKeys("31");	
		driver.findElement(By.xpath("//input[@id='salary']")).sendKeys("64000");	
		driver.findElement(By.xpath("//input[@id='department']")).sendKeys("QA");	
		driver.findElement(By.xpath("//button[@id='submit']")).click();
	}
	@Test(priority = 2)
	public void fetchTheRecords() {
		waitActivity.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[name()='path' and contains(@d,'M16 132h41')]")))).click();
		driver.findElement(By.xpath("//*[text()='Web Tables']")).click();
		
		List<WebElement> rd=driver.findElements(By.xpath("//div[@class='rt-td']"));
		for(WebElement r: rd) {
			if(!r.getText().isBlank()) {
				System.out.println(r.getText());
			}
		}
	}
	
	@Test(priority = 3)
	public void updateExistRecord() {
		waitActivity.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[name()='path' and contains(@d,'M16 132h41')]")))).click();
		driver.findElement(By.xpath("//*[text()='Web Tables']")).click();
		driver.findElement(By.xpath("//span[@id='edit-record-1']//*[name()='svg']//*[name()='path' and contains(@d,'M880 836H1')]")).click();
		WebElement fName=driver.findElement(By.xpath("//input[@id='firstName']"));
		fName.clear();
		fName.sendKeys("Sumit");
		WebElement lName=driver.findElement(By.xpath("//input[@id='lastName']"));
		lName.clear();
		lName.sendKeys("Kumar");
		WebElement email=driver.findElement(By.xpath("//input[@id='userEmail']"));
		email.clear();
		email.sendKeys("Sumit@test.com");
		WebElement age=driver.findElement(By.xpath("//input[@id='age']"));
		age.clear();
		age.sendKeys("31");
		WebElement salary=driver.findElement(By.xpath("//input[@id='salary']"));
		salary.clear();
		salary.sendKeys("64000");
		WebElement department=driver.findElement(By.xpath("//input[@id='department']"));
		department.clear();
		department.sendKeys("QA");
		driver.findElement(By.xpath("//button[@id='submit']")).click();

	}
	@Test(priority = 4)
	public void deleteRecord() {
		waitActivity.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[name()='path' and contains(@d,'M16 132h41')]")))).click();
		driver.findElement(By.xpath("//*[text()='Web Tables']")).click();
		driver.findElement(By.xpath("//span[@id='delete-record-1']//*[name()='svg']//*[name()='path' and contains(@d,'M864 256H7')]")).click();
			}
}
