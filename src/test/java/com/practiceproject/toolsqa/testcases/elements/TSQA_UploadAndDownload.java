/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 19-Apr-2022
 *  FILE NAME  		: 	 TSQA_UploadAndDownload.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    2:04:04 pm
 */
package com.practiceproject.toolsqa.testcases.elements;

import java.io.File;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

import com.practiceproject.toolsqa.basepage.ToolsQA_BaseClass;
import com.practiceproject.toolsqa.utils.TSQA_CommonControllers;

public class TSQA_UploadAndDownload extends ToolsQA_BaseClass {
	//@Test
	public void verifyDownload() {
		driver.get("https://demoqa.com/upload-download");
		driver.findElement(By.xpath("//a[@id='downloadButton']")).click();
		boolean fileStatus=TSQA_CommonControllers.isFileDownloadingSuccessByName("C:\\Users\\sumit.chaudhary\\Downloads", "sampleFile.jpeg");
		System.out.println(fileStatus);
	}
	
	@Test
	public void verifyUpload(){
		driver.get("https://demoqa.com/upload-download");
		driver.findElement(By.xpath("//input[@id='uploadFile']")).sendKeys(System.getProperty("user.dir")+File.separator+"ExcelFils"+File.separator+"ExcelWriteRead.xls");
		System.out.println("Uploaded File Path is: "+driver.findElement(By.xpath("//p[@id='uploadedFilePath']")).getText());
	}
	
	

}
