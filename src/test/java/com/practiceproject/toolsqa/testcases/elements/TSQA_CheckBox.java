/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 14-Apr-2022
 *  FILE NAME  		: 	 TSQA_CheckBox.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    11:31:09 pm
 */
package com.practiceproject.toolsqa.testcases.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.practiceproject.toolsqa.basepage.ToolsQA_BaseClass;

public class TSQA_CheckBox extends ToolsQA_BaseClass{
	
	@Test
	public void handleCheckBox() {
		driver.get("https://demoqa.com/checkbox");
		driver.findElement(By.xpath("//div[@class='element-list collapse show']//li[@id='item-1']")).click();
		driver.findElement(By.xpath("//button[@title='Toggle']//*[name()='svg']")).click();
		driver.findElement(By.xpath("//li[2]//span[1]//button[1]//*[name()='svg']")).click();
		driver.findElement(By.xpath("//body//div[@id='app']//li[@class='rct-node rct-node-parent rct-node-expanded']//li[@class='rct-node rct-node-parent rct-node-expanded']//li[1]//span[1]//button[1]//*[name()='svg']")).click();
		driver.findElement(By.xpath("//label[@for='tree-node-react']//span[@class='rct-checkbox']//*[name()='svg']")).click();
		String successMsg = driver.findElement(By.xpath("//span[@class='text-success']")).getText();
		Assert.assertEquals("react", successMsg);
	}
}
