/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 14-Apr-2022
 *  FILE NAME  		: 	 TSQA_TextBox.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    5:45:17 pm
 */
package com.practiceproject.toolsqa.testcases.elements;

import java.time.Duration;
import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import com.practiceproject.toolsqa.basepage.ToolsQA_BaseClass;

public class TSQA_TextBox extends ToolsQA_BaseClass{
	
	@Test
	public void handleTextBox() {
		waitActivity.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//*[name()='path' and contains(@d,'M16 132h41')]")))).click();
	 	driver.findElement(By.xpath("//span[normalize-space()='Text Box']")).click();
		driver.findElement(By.xpath("//input[@id='userName']")).sendKeys("Sumit Kumar");
		driver.findElement(By.xpath("//input[@id='userEmail']")).sendKeys("sumttest@email.com");
		WebElement currAdd=driver.findElement(By.xpath("//textarea[@id='currentAddress']"));
		
		currAdd.sendKeys("Village- Sarila, District - Hamirpur, State- UP, 210432");
	
		//Copy the address from current address text area
		actionActivity.keyDown(Keys.CONTROL);
		actionActivity.sendKeys("a");
		actionActivity.keyUp(Keys.CONTROL);
		actionActivity.build().perform();
		actionActivity.keyDown(Keys.CONTROL);
		actionActivity.sendKeys("c");
		actionActivity.keyUp(Keys.CONTROL);
		actionActivity.build().perform();
		
		
		//shift current address to permanent address
		actionActivity.sendKeys(Keys.TAB);
		actionActivity.build().perform();
		
		//WebElement permanentAdd=driver.findElement(By.xpath("//textarea[@id='permanentAddress']"));
		//Past the copied text into permanent add text area
		actionActivity.keyDown(Keys.CONTROL);
		actionActivity.sendKeys("V");
		actionActivity.keyUp(Keys.CONTROL);
		actionActivity.build().perform();
		scrollPage("500");
		waitActivity.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//button[@id='submit']")))).click();
		
		String output=driver.findElement(By.xpath("//div[@id='output']")).getText();
		System.out.println(output);
	
	}

}
