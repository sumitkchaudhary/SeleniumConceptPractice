/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 18-Apr-2022
 *  FILE NAME  		: 	 TSQA_BrokenLinks.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    10:53:51 am
 */
package com.practiceproject.toolsqa.testcases.elements;


import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import com.practiceproject.toolsqa.basepage.ToolsQA_BaseClass;

public class TSQA_BrokenLinks extends ToolsQA_BaseClass {
	
	
	@Test()
    protected void verifyTheBrokenLinks() {
    	driver.get("https://demoqa.com/broken");
		List<WebElement> imageLinks=driver.findElements(By.tagName("img"));
		for(WebElement brokenImage: imageLinks) {
			
			verifyLinks(brokenImage.getAttribute("src"));
			//Validate image display using JavaScript executor
            try {
                boolean imageDisplayed = (Boolean) ((JavascriptExecutor) driver).executeScript("return (typeof arguments[0].naturalWidth !=\"undefined\" && arguments[0].naturalWidth > 0);", brokenImage);
                if (imageDisplayed) {
                    System.out.println("DISPLAY - OK "+brokenImage.getAttribute("src"));
                }else {
                     System.out.println("DISPLAY - BROKEN "+brokenImage.getAttribute("src"));
                }
            } 
            catch (Exception e) {
            	System.out.println("Error Occured");
            }
		}
	
    }
    
    public static void verifyLinks(String linkUrl)
    {
        try
        {
            URL url = new URL(linkUrl);

            //Now we will be creating url connection and getting the response code
            HttpURLConnection httpURLConnect=(HttpURLConnection)url.openConnection();
            httpURLConnect.setConnectTimeout(5000);
            httpURLConnect.connect();
            if(httpURLConnect.getResponseCode()!=200)
            {
            	System.out.println("HTTP STATUS - " + httpURLConnect.getResponseMessage() + " is a broken link");
            }    
       
            //Fetching and Printing the response code obtained
            else{
                System.out.println("HTTP STATUS - " + httpURLConnect.getResponseMessage());
            }
        }catch (Exception e) {
        	e.getStackTrace();
        	e.getMessage();
      }
   }
  

}
