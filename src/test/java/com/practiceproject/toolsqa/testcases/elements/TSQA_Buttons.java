/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 16-Apr-2022
 *  FILE NAME  		: 	 TSQA_Buttons.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    5:12:40 pm
 */
package com.practiceproject.toolsqa.testcases.elements;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.practiceproject.toolsqa.basepage.ToolsQA_BaseClass;

public class TSQA_Buttons extends ToolsQA_BaseClass{
	
	@Test
	public void validateDoubleClick() {
		driver.get("https://demoqa.com/buttons");
		actionActivity.doubleClick(driver.findElement(By.xpath("//button[@id='doubleClickBtn']"))).build().perform();
		String doubleClickConfirmMessage=driver.findElement(By.xpath("//p[@id='doubleClickMessage']")).getText();
		Assert.assertEquals("You have done a double click",doubleClickConfirmMessage);

	}

	@Test
	public void validateRightClick(){
		driver.get("https://demoqa.com/buttons");
		actionActivity.contextClick(driver.findElement(By.xpath("//button[@id='rightClickBtn']"))).perform();
		String rightClickConfirmMessage=driver.findElement(By.xpath("//p[@id='rightClickMessage']")).getText();
		Assert.assertEquals("You have done a right click",rightClickConfirmMessage);
	}
	@Test void validateClickMe(){
		driver.get("https://demoqa.com/buttons");
		driver.findElement(By.xpath("//*[text()='Click Me']")).click();
		String clickMeConfirmMessage=driver.findElement(By.xpath("//p[@id='dynamicClickMessage']")).getText();
		Assert.assertEquals("You have done a dynamic click",clickMeConfirmMessage);
	}
}
