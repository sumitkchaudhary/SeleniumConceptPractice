/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 20-May-2023
 *  FILE NAME  		: 	 TSQA_CheckBoxes.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    5:55:03 pm
 */
package com.practiceproject.toolsqa.testcases.wedgets;

import com.practiceproject.toolsqa.basepage.ToolsQA_BaseClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class TSQA_CheckBoxes extends ToolsQA_BaseClass {
	@Test
	public void verifyTheCheckBoxClick() {
		driver.navigate().to("https://demoqa.com/checkbox");
		driver.findElement(By.xpath("//button[@title='Toggle']//*[name()='svg']")).click();
		driver.findElement(By.xpath("//li[@class='rct-node rct-node-parent rct-node-expanded']//li[1]//span[1]//button[1]//*[name()='svg']")).click();
		driver.findElement(By.xpath("(//span[@class='rct-checkbox'])[3]")).click();
	}

}
