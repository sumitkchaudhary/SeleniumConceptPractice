/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 22-Apr-2022
 *  FILE NAME  		: 	 TSQA_ControlDatePicker.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    3:35:03 pm
 */
package com.practiceproject.toolsqa.testcases.wedgets;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.practiceproject.toolsqa.basepage.ToolsQA_BaseClass;
import com.practiceproject.toolsqa.utils.TSQA_ControlDatePicker;

public class TSQA_DatePicker extends ToolsQA_BaseClass{
	
	@Test
	public void verifySelectDate() {
		driver.get("https://demoqa.com/date-picker");
		
		WebElement date=driver.findElement(By.xpath("//input[@id='datePickerMonthYearInput']"));
		date.click();
		TSQA_ControlDatePicker.desireDatePicker("January","31", "2022");
		System.out.println(date.getAttribute("value"));
	}
	@Test
	public void verifySelectDateWithTime() {
		//Open webSite
		driver.get("https://demoqa.com/date-picker");
		//Store the desire element
		WebElement dateTime=driver.findElement(By.xpath("//input[@id='dateAndTimePickerInput']"));
		//Call action classes
		//Select all visible text by press CTRL + A and Delete 
		actionActivity.keyDown(dateTime,Keys.CONTROL).sendKeys("A").keyUp(Keys.CONTROL).sendKeys(Keys.DELETE).build().perform();
		//Send new Date
		actionActivity.sendKeys("June 17, 1990 6:30 AM");
		//Press Enter
		actionActivity.sendKeys(Keys.ENTER);
		
	}
	
}
