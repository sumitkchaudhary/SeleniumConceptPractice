/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 26-Apr-2022
 *  FILE NAME  		: 	 TSQA_ProgressBar.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    10:35:18 am
 */
package com.practiceproject.toolsqa.testcases.wedgets;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import com.practiceproject.toolsqa.basepage.ToolsQA_BaseClass;

public class TSQA_ProgressBar extends ToolsQA_BaseClass{
	
	@Test
	public void verifyProgressBar() {
		driver.get("https://demoqa.com/progress-bar");
		driver.findElement(By.xpath("//button[text()='Start']")).click();
		
		WebElement progressBar=driver.findElement(By.xpath("//div[@role='progressbar']"));
		
		boolean checkProgressValue=waitActivity.until(ExpectedConditions.attributeToBe(progressBar, "aria-valuenow", "100"));
		if(checkProgressValue==true) {
			waitActivity.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[text()='Reset']"))).click();
		}
	}
}
