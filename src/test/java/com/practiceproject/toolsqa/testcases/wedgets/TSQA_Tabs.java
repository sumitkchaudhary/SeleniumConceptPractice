/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 26-Apr-2022
 *  FILE NAME  		: 	 TSQA_Tabs.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    2:55:28 pm
 */
package com.practiceproject.toolsqa.testcases.wedgets;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.practiceproject.toolsqa.basepage.ToolsQA_BaseClass;

public class TSQA_Tabs extends ToolsQA_BaseClass{
	@Test
	public void verifyTabs() {
		driver.get("https://demoqa.com/tabs");
		List<WebElement> tabs= driver.findElements(By.xpath("//a[@role='tab']"));
		for(WebElement tab: tabs) {
			if(tab.getText().equals("More")) {
				return;
			}
			tab.click();
			List<WebElement> tabContents=driver.findElements(By.xpath("//div[@role='tabpanel']"));
			for(WebElement tabCon:tabContents) {
				System.out.println(tabCon.getText());
			}
			
		}
	}

}
