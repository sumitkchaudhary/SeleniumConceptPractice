/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 26-Apr-2022
 *  FILE NAME  		: 	 TSQA_ToolTips.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    3:14:51 pm
 */
package com.practiceproject.toolsqa.testcases.wedgets;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;

import com.practiceproject.toolsqa.basepage.ToolsQA_BaseClass;

public class TSQA_ToolTips extends ToolsQA_BaseClass {
	
	@Test
	public void handlingToolsTips() {
		driver.get("https://demoqa.com/tool-tips");
		WebElement hoverMeTose= driver.findElement(By.xpath("//button[@id='toolTipButton']"));
		
		actionActivity.moveToElement(hoverMeTose).perform();
		// textFiledToolTip
		
		WebElement textGet=driver.findElement(By.xpath("//div[@id='buttonToolTip']"));
		
		String hoverMessage=waitActivity.until(ExpectedConditions.visibilityOf(textGet)).getText();
		System.out.println(hoverMessage);
		
	}

}
