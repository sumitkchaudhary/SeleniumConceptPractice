/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 26-May-2022
 *  FILE NAME  		: 	 TSQA_SelectMenu.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    11:21:26 pm
 */
package com.practiceproject.toolsqa.testcases.wedgets;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.practiceproject.toolsqa.basepage.ToolsQA_BaseClass;

public class TSQA_SelectMenu extends ToolsQA_BaseClass {
	
	@Test
	public void SelectValue() {
		driver.get("https://demoqa.com/select-menu");
		driver.findElement(By.xpath("//div[@id='withOptGroup']")).click();
		driver.findElement(By.xpath("//*[text()='Group 2, option 2']")).click();
		
		driver.findElement(By.xpath("//div[@id='selectOne']")).click();
		driver.findElement(By.xpath("//*[text()='Mrs.']")).click();
		
		WebElement selectOld=driver.findElement(By.xpath("//select[@id='oldSelectMenu']"));
		Select selectOldList=new Select(selectOld);
		selectOldList.selectByVisibleText("White");
	}

}
