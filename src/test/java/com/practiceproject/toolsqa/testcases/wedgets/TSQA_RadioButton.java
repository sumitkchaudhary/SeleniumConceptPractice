/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 20-May-2023
 *  FILE NAME  		: 	 TSQA_RadioButton.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    7:23:12 pm
 */
package com.practiceproject.toolsqa.testcases.wedgets;

import com.practiceproject.toolsqa.basepage.ToolsQA_BaseClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class TSQA_RadioButton extends ToolsQA_BaseClass {
	@Test
	public void verifyTheCheckBoxClick() {
		driver.navigate().to("https://demoqa.com/radio-button");
		driver.findElement(By.xpath("//label[normalize-space()='Yes']")).click();
		String actualMessage=driver.findElement(By.xpath("//span[@class='text-success']")).getText();
		Assert.assertEquals(actualMessage, "Yes");
	}
}
