/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 22-Apr-2022
 *  FILE NAME  		: 	 TSQA_AutoComplete.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    1:22:35 pm
 */
package com.practiceproject.toolsqa.testcases.wedgets;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.practiceproject.toolsqa.basepage.ToolsQA_BaseClass;

public class TSQA_AutoComplete extends ToolsQA_BaseClass{
	String colorNameList[]= {"Red","Green","Blue","Black","White","Yellow","Voilet","Indigo","Magenta","Aqua","Purple"};
	
//	@Test
	public void verifyMultiColorSelector() throws InterruptedException {
		driver.get("https://demoqa.com/auto-complete");
		WebElement multiSelect= driver.findElement(By.xpath("//input[@id='autoCompleteMultipleInput']"));
		for(String color: colorNameList) {
			multiSelect.sendKeys(color);
			Thread.sleep(200);
			multiSelect.sendKeys(Keys.ARROW_DOWN);
			Thread.sleep(200);
			multiSelect.sendKeys(Keys.ENTER);
			Thread.sleep(200);
		}
	}
	@Test
	public void verifySingleColorSelector() throws InterruptedException {
		driver.get("https://demoqa.com/auto-complete");
		WebElement multiSelect= driver.findElement(By.xpath("//input[@id='autoCompleteSingleInput']"));
		for(String color: colorNameList) {
			multiSelect.sendKeys(color);
			Thread.sleep(200);
			multiSelect.sendKeys(Keys.ARROW_DOWN);
			Thread.sleep(200);
			multiSelect.sendKeys(Keys.ENTER);
			Thread.sleep(200);
		}
		
		
	}

}
