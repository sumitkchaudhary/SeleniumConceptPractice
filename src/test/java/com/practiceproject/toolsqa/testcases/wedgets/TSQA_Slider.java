/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 25-Apr-2022
 *  FILE NAME  		: 	 TSQA_Slider.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    6:36:36 pm
 */
package com.practiceproject.toolsqa.testcases.wedgets;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

import com.practiceproject.toolsqa.basepage.ToolsQA_BaseClass;

public class TSQA_Slider extends ToolsQA_BaseClass{
	
	@Test
	public void verifySlider(){
		driver.get("https://demoqa.com/slider");
		WebElement range= driver.findElement(By.xpath("//input[@type='range']"));
		 
		System.out.println("X "+range.getLocation().getX());
		System.out.println("Y "+range.getLocation().getY());
		/*xOffset horizontal move 
		 *yOffset vertical move offset.*/
		actionActivity.dragAndDropBy(range, 800, range.getLocation().getY()).build().perform();
		if(driver.findElement(By.xpath("//input[@id='sliderValue']")).getAttribute("value").equals("100")) {
			System.out.println("Test Case Pass");
		}else {
			System.out.println("Test Case Failed");
		}
		
	}
}
