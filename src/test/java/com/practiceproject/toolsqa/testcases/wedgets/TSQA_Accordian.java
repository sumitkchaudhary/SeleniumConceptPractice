/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 22-Apr-2022
 *  FILE NAME  		: 	 TSQA_Accordian.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    12:23:55 pm
 */
package com.practiceproject.toolsqa.testcases.wedgets;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;
import com.practiceproject.toolsqa.basepage.ToolsQA_BaseClass;

public class TSQA_Accordian extends ToolsQA_BaseClass{
	//@Test
	public void verifyAccordian() throws InterruptedException {
		driver.get("https://demoqa.com/accordian");
		
		String sectionOneContent=driver.findElement(By.xpath("//div[@id='section1Content']//p")).getText();
		driver.findElement(By.xpath("//div[@id='section1Heading']")).click();
		System.out.println(sectionOneContent);
		Thread.sleep(300);
		WebElement section2=driver.findElement(By.xpath("//div[@id='section2Heading']"));
		section2.click();
		String sectionTwoContent=driver.findElement(By.xpath("//div[@id='section2Content']//p")).getText();
		System.out.println(sectionTwoContent);
		section2.click();
		Thread.sleep(300);
		WebElement section3=driver.findElement(By.xpath("//div[@id='section3Heading']"));
		section3.click();
		String sectionThreeContent=waitActivity.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@id='section3Content']//p")))).getText();
		System.out.println(sectionThreeContent);
	}
	
}
