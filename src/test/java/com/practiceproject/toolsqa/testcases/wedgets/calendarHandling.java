/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 21-May-2023
 *  FILE NAME  		: 	 calendarHandling.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    12:06:38 am
 */
package com.practiceproject.toolsqa.testcases.wedgets;

import com.practiceproject.toolsqa.basepage.ToolsQA_BaseClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.testng.annotations.Test;

public class calendarHandling extends ToolsQA_BaseClass {
	@Test
	public void validateDateSelection() {
		driver.navigate().to("https://demoqa.com/date-picker");

		driver.findElement(By.xpath("//input[@id='datePickerMonthYearInput']")).click();
		datePicker("June", "1990","17");		
	}
	public static void datePicker(String month, String year, String date) {
		WebElement monthSelection=driver.findElement(By.xpath("//select[@class='react-datepicker__month-select']"));
		WebElement yearSelection=driver.findElement(By.xpath("//select[@class='react-datepicker__year-select']"));
		Select monthDropDown=new Select(monthSelection);
		
		monthDropDown.selectByVisibleText(month);
		Select yearDropDown=new Select(yearSelection);
		yearDropDown.selectByVisibleText(year);
		
		driver.findElement(By.xpath("//*[text()='"+date+"']")).click();

	}

}
