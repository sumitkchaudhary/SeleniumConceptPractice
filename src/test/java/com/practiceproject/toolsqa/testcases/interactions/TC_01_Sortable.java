/**
 * @author 			:	 Sumit chaudhary
 *	DATE       		:	 Apr 21, 2020
 *  FILE NAME  		: 	 TC_01_Sortable.java
 *  PROJECT NAME 	:	 SeleniumInterviewPractice
 * 	Class Time		:    8:28:19 PM
 */
package com.practiceproject.toolsqa.testcases.interactions;

import com.SeleniumConcept.BaseUtilities.MasterControllers;
import com.practiceproject.toolsqa.basepage.ToolsQA_BaseClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

public class TC_01_Sortable extends ToolsQA_BaseClass {
	@Test
	public void sortableTest() {
		WebElement shortableElement=driver.findElement(By.xpath("//a[contains(text(),'Sortable')]"));
		shortableElement.click();

		if (driver.getCurrentUrl().equals("https://demoqa.com/sortable/")) {
			System.out.println("Sortable window successfully open");
		} else {
			System.out.println("Please check the element");
		}

		driver.findElement(By.xpath("//li[contains(text(),'Item 1')]"));
	}
}
