/**
 * @author 			:	 sumit.chaudhary
 *	DATE       		:	 14-Apr-2022
 *  FILE NAME  		: 	 ToolsQA_BaseClass.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    5:34:07 pm
 */
package com.practiceproject.toolsqa.basepage;

import java.time.Duration;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import com.SeleniumConcept.BaseUtilities.MasterControllers;

public class ToolsQA_BaseClass extends MasterControllers {
	
	public WebDriverWait waitActivity;
	public Actions actionActivity;

	@BeforeMethod
	public void launchToolsQADemoSite() {
		launchBrowserController();
		driver.get("https://demoqa.com/");
		driver.manage().window().maximize();
		scrollPage("500");
		//driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));//Wait for 1 minute
		waitActivity=new WebDriverWait(driver, Duration.ofSeconds(60)); 
		actionActivity=new Actions(driver);
		
	}
	
	@AfterMethod
	public void quitFromToolsSQADemoSite() {
		try {
			Thread.sleep(6000);
			driver.quit();
		} catch (Exception e) {
			e.getMessage();
		}
	}
	
	public static void scrollPage(String tillDown) {
		JavascriptExecutor executeJs=(JavascriptExecutor)driver;
		executeJs.executeScript("scrollBy(0, "+tillDown+")");
	}
	
	public static void scrollPage(WebElement exElement) {
		Actions actions = new Actions(driver);
		actions.moveToElement(exElement);
		actions.perform();
	}

}
