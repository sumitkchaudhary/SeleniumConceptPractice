/**
 * @author 			:	 SumitChaudhary
 *	DATE       		:	 25-Jan-2023
 *  FILE NAME  		: 	 ExtractIntandStrValueFromString.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    7:12:19 am
 */
package com.SeleniumConcept.BaseUtilities;

import java.time.LocalTime;

public class ExtractIntandStrValueFromString {
	public static String intValueOfString;
	public static String strValueOfString;
	
	public static String getIntValueOfString() {
		return intValueOfString;
	}
	public void setIntValueOfString(String intValueOfString) {
		ExtractIntandStrValueFromString.intValueOfString = intValueOfString;
	}
	public static String getStrValueOfString() {
		return strValueOfString;
	}
	public void setStrValueOfString(String strValueOfString) {
		ExtractIntandStrValueFromString.strValueOfString = strValueOfString;
	}
	public static ExtractIntandStrValueFromString getIntAndStrFromString(String s) {
		ExtractIntandStrValueFromString obj=new ExtractIntandStrValueFromString();
		obj.setIntValueOfString(s.replaceAll("[^0-9]", ""));
		obj.setStrValueOfString(s.replaceAll("[^a-zA-Z]", ""));
		return obj;
	}
	
	public static void main(String[] args) {
		String futureTime="8";
		
		LocalTime time=LocalTime.parse("19:34:50.63");
		System.out.println(time.getMinute());
		
	}


}
