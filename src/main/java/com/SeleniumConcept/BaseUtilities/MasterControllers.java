/**
 * @author 			:	 dell
 *	DATE       		:	 19-Jan-2022
 *  FILE NAME  		: 	 BrowserConfigurationController.java
 *  PROJECT NAME 	:	 SeleniumConceptPractice
 * 	Class Time		:    11:48:58 pm
 */
package com.SeleniumConcept.BaseUtilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import io.github.bonigarcia.wdm.WebDriverManager;

public class MasterControllers {
	public static WebDriver driver;
	public static void launchBrowserController() {
		String browserName= getPropertiesFileValue("config.properties").getProperty("browserName");
		if(browserName.contains("chrome")) {
			ChromeOptions notification=new ChromeOptions();
			WebDriverManager.chromedriver().setup();
			notification.addArguments("--disable-notifications");
			driver =new ChromeDriver(notification);
			
		}else if(browserName.contains("firfox")|| browserName.contains("mozilla")) {
			FirefoxBinary firefoxBinary = new FirefoxBinary(new File("C:"+File.separator+"Users"+File.separator+System.getProperty("user.name")+File.separator+"AppData"+File.separator+"Local"+File.separator+"Mozilla Firefox"+File.separator+"firefox.exe"));
			//Set the desire capabilities 
			DesiredCapabilities desired = new DesiredCapabilities();
			//Set the fire fox options
			FirefoxOptions options = new FirefoxOptions();
			desired.setCapability(FirefoxOptions.FIREFOX_OPTIONS, options.setBinary(firefoxBinary));
		
			options.setProfile(new FirefoxProfile());
			options.addPreference("dom.webnotifications.enabled", false);
			WebDriverManager.firefoxdriver().setup();
			driver =new FirefoxDriver();
			
		}else if(browserName.contains("edge")) {
			EdgeOptions notification=new EdgeOptions();
			notification.setCapability("--disable-notifications", true);
			WebDriverManager.edgedriver().setup();
			driver =new EdgeDriver(notification);
			
		}else if(browserName.contains("IE")||browserName.contains("Explorer")) {
			InternetExplorerOptions notification=new InternetExplorerOptions();
			notification.setCapability("--disable-notifications", true);
			WebDriverManager.iedriver().setup();
			driver =new InternetExplorerDriver(notification);
			
		}else if(browserName.contains("brave")) {
			ChromeOptions braveExePath=new ChromeOptions();
			
			WebDriverManager.chromedriver().setup();
			braveExePath.setBinary(new File("C:\\Program Files\\BraveSoftware\\Brave-Browser\\Application\\brave.exe"));
			driver =new ChromeDriver(braveExePath);
			
		}
		else {
			System.out.println("Please check the browser name which you provide it should be : \n "+browserName+" Setting is not configured for this browser.");
			System.out.println("1. chrome \n 2. firfox or mozilla \n 3. edge 4. IE Or Explorer \n 4. brave");
		}
	}

	public static Properties getPropertiesFileValue(String fileName){
		Properties propertiesFileValue=new Properties();
		try{
			FileInputStream fos=new FileInputStream(new File(getAbsolutPath(fileName)));
			propertiesFileValue.load(fos);
		}catch (Exception e){
			e.printStackTrace();
			e.getMessage();
		}
		return propertiesFileValue;
	}

	public static String getAbsolutPath(String fileName) {

		String fileAbolutePath ="";
		try {
			//Collection method
			Collection<File> files = FileUtils.listFiles(new File(System.getProperty("user.dir")), null, true);
			for (Iterator<File> iterator = files.iterator(); iterator.hasNext();) {
				File file = (File) iterator.next();
				if (file.getName().equals(fileName))
					fileAbolutePath=file.getAbsolutePath();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileAbolutePath;
	}
	public static String captureScreen(WebDriver driver, String screenShotName) throws IOException {
		TakesScreenshot ts= (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		String scDestination = System.getProperty("user.dir") + File.separator+ System.currentTimeMillis() + ".png";
		File destination = new File(scDestination);
		FileUtils.copyFile(source, destination);

		return scDestination;
	}
}
